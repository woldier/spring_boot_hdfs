package com.cdut.hadoop.controller;


import com.cdut.hadoop.common.R;
import com.cdut.hadoop.service.HdfsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

import static com.cdut.hadoop.common.PathRegxUtil.resolvePath;


@Controller
@RequestMapping("/api/hadoop/hdfs")
public class HdfsController {
    // 日志记录
    private static final Logger logger = LoggerFactory.getLogger(HdfsController.class);
    @Autowired
    private HdfsService service;

    @PutMapping("/{path}")
    @ResponseBody
    public R mkdirFolder(@PathVariable(name = "path") String path) {
        path = resolvePath(path);

        boolean b = service.mkdirFolder(path);
        return b ? R.success("成功创建") : R.error("创建失败");
    }

    /**
     * HDFS 文件是否存在
     *
     * @param path
     * @return
     */
    @GetMapping("/exist/{path}")
    @ResponseBody
    public R existFile(@PathVariable(name = "path") String path) {
        path = resolvePath(path);
        boolean b = service.existFile(path);
        return b ? R.success("文件夹存在") : R.error("文件夹不存在");
    }


    /**
     * HDFS 读取目录信息
     *
     * @param path
     * @return
     */
    @PostMapping("/{path}")
    @ResponseBody
    public R readCatalog(@PathVariable(name = "path") String path) {
        path = resolvePath(path);

        List<Map<String, Object>> list = service.readCatalog(path);

        return list != null ? R.success(list) : R.error("读取目录失败");

    }


    /**
     * HDFS 读完文件列表
     *
     * @param path
     * @return
     */
    @PatchMapping("/{path}")
    @ResponseBody
    public R listFile(@PathVariable(name = "path") String path) {
        path = resolvePath(path);
        List<Map<String, String>> list = service.listFile(path);
        return list != null ? R.success(list) : R.error("error");

    }


    /**
     * 文件，文件夹重命名
     * @param oldName
     * @param newName
     * @return
     */

    @GetMapping("/rename/{oldName}/{newName}")
    @ResponseBody
    public R renameFile(
            @PathVariable(name = "oldName")String oldName,
            @PathVariable(name = "newName")String newName) {
        oldName =   resolvePath(oldName);
        newName =   resolvePath(newName);
        boolean b = service.renameFile(oldName, newName);
        return b?R.success(""):R.error("");
    }

    /**
     * HDFS 文件删除
     * @param path
     * @return
     */
    @DeleteMapping("/{path}")
    @ResponseBody
    public R deleteFile( @PathVariable(name = "path") String path){
        path = resolvePath(path);
        boolean b = service.deleteFile(path);
        return b?R.success(""):R.error("");
    }

    @PutMapping("/{path}/{uploadPath}")
    @ResponseBody
    public R uploadFile(
            @PathVariable(name = "path")String path,
            @PathVariable(name = "uploadPath")String uploadPath) {
        path = resolvePath(path);
        uploadPath = resolvePath(uploadPath);

        service.uploadFile(path,uploadPath);
        return R.success("成功");
    }


    @PostMapping("/{path}/{downloadPath}")
    @ResponseBody
    public R downloadFile(@PathVariable(name = "path")String path,
                          @PathVariable(name = "downloadPath")String downloadPath) {
        path = resolvePath(path);
        downloadPath = resolvePath(downloadPath);
        service.downloadFile(path,downloadPath);
        return R.success("成功");
    }


}
