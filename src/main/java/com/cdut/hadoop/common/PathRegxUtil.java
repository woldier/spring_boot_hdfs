package com.cdut.hadoop.common;

/**
 * 此类的作用是对请求中的路径进行预处理
 *  如 请求路径 为/demo/sub
 *  在http请求中为 http：baseurl/demo-sub(RestFul风格)
 *  而hdfs中为传统表示方法 因此我们需要对其进行一定的处理
 *
 *
 *  存在的问题 不允许目录名或者文件名中出现 “ - ”  ，否则狗导致错误
 */
public class PathRegxUtil {
    public static String resolvePath(String rex){
        String s = rex.replaceAll("-", "//");
        return s;
    }
}
